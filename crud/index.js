const express=require("express")
const app=express()
const connectMongodb=require("./config")
const cors=require("cors")
const PORT =3000


const {graphqlHTTP}=require("express-graphql")
// Getting schema object
const schema=require("./schemas/schema")

connectMongodb();
app.use(express.json())
app.use(cors())
app.use('/graphql',graphqlHTTP({
    schema,
    graphiql:true
}))


app.listen(PORT,()=>{
    console.log("Sever is running");
})