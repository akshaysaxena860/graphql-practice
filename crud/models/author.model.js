const mongoose=require("mongoose");


const authorModel=new mongoose.Schema({
    name: String,
    age: Number
})

module.exports=mongoose.model('Author',authorModel);