const mongoose = require("mongoose");

const connectMongodb = () => {
  mongoose.connect(
    "mongodb+srv://graphqltest:graphqltest@cluster0.au66p.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
  );
  mongoose.connection.once("open", () => {
    console.log("Connected to Mongodb Atlas");
  });
};

module.exports=connectMongodb;